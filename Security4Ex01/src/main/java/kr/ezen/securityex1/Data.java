package kr.ezen.securityex1;

@lombok.Data
public class Data {
	private String name;
	private String password;
	private String content;
}
